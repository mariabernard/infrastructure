# playbook to install galaxy
---

- name: gxadmin
  hosts: galaxy
  pre_tasks:
    - name: Install Dependencies
      package:
        name: "{{ item }}"
      become: yes
      with_items:
        - postgresql-client
        - libxml2-utils
    - name: Copy gxadmin slurp script for telegraf
      template:
        src: "galaxy/gxadmin_slurp.sh.j2"
        dest: "{{ gxadmin_bin_dir }}/gxadmin_slurp.sh"
        mode: 0750
        owner: "telegraf"
        group: "telegraf"
  roles:
    - role: gxadmin-extra
      become: true
      become_user: "{{ galaxy_user }}"
    - role: galaxyproject.gxadmin
    - role: dj-wasabi.telegraf

- name: tiaas
  hosts: galaxy
  pre_tasks:
    - name: Install Dependencies
      package:
        name: "{{ item }}"
      become: yes
      with_items:
        - python3-dev
  roles:
    - role: galaxyproject.tiaas2

- name: galaxy cron
  hosts: galaxy
  tasks:
    - name: galaxy.yml symlink in server/config/
      file:
        src: "{{ galaxy_config_dir }}/galaxy.yml"
        dest: "{{ galaxy_server_dir }}/config/galaxy.yml"
        state: link
    - block:
      - name: cron cleanup_datasets
        cron:
          name: "galaxy cleanup "
          minute: "0"
          hour: "4"
          job: "
            . {{ galaxy_venv_dir }}/bin/activate && cd {{ galaxy_server_dir }} &&
            (
            ./scripts/cleanup_datasets/cleanup_datasets.py -1 -r -d 90;
            ./scripts/cleanup_datasets/cleanup_datasets.py -6 -r -d 14;
            ./scripts/cleanup_datasets/cleanup_datasets.py -2 -r -d 14;
            ./scripts/cleanup_datasets/cleanup_datasets.py -4 -r -d 14;
            ./scripts/cleanup_datasets/cleanup_datasets.py -5 -r -d 14;
            ./scripts/cleanup_datasets/cleanup_datasets.py -3 -r -d 14
            ) >>  {{ galaxy_log_dir }}/cleanup_datasets.log
            "
      - name: cron cleanup tmp
        # Not used anymore in theory
        cron:
          name: "galaxy cleanup galaxy_root/tmp"
          minute: "0"
          hour: "2"
          job: 'find "{{ galaxy_root }}/tmp/" -mtime +15 -exec rm {} \;'
          state: absent
      - name: cron cleanup tmp mutable
        # The new tmp dir location
        cron:
          name: "galaxy cleanup galaxy_mutable/tmp"
          minute: "0"
          hour: "2"
          job: 'find "{{ galaxy_mutable_data_dir }}/tmp/" -mindepth 1 -mtime +15 -exec rm -rf {} \;'
      - name: cron cleanup tmp upload
        # Temp file for uploads with tusd
        cron:
          name: "galaxy cleanup galaxy_root/tmp_upload"
          minute: "0"
          hour: "2"
          job: 'find "{{ galaxy_root }}/tmp_upload/" -mindepth 1 -mtime +15 -exec rm {} \;'
      - name: cron cleanup galaxy_job_working_directory
        # Job temp dir
        cron:
          name: "galaxy cleanup galaxy_job_working_directory"
          minute: "0"
          hour: "2"
          job: 'find "{{ galaxy_root }}/jobs/" -mindepth 1 -mtime +15 -exec rm -rf {} \;'
          state: absent
      - name: cron cleanup galaxy ftp
        # FTP dir
        cron:
          name: "galaxy cleanup ftp"
          minute: "0"
          hour: "2"
          job: 'find "{{ galaxy_ftp_upload_dir }}" -mindepth 2 -mtime +15 -exec rm -rf {} \;'
      become: yes
      become_user: "{{ galaxy_user }}"
    - name: cron cleanup galaxy_job_working_directory
      # Job temp dir as root
      cron:
        name: "galaxy cleanup galaxy_job_working_directory"
        minute: "0"
        hour: "2"
        job: 'find "{{ galaxy_root }}/jobs/" -mindepth 1 -mtime +15 -exec rm -rf {} \;'
      become: yes
    - name: cron set_user_disk_usage
      # To correct user quota
      cron:
        name: "galaxy set_user_disk_usage "
        day: "1,14"
        job: "
          . {{ galaxy_venv_dir }}/bin/activate && cd {{ galaxy_server_dir }} &&
          (
          ./scripts/set_user_disk_usage.py ;
          ) >>  {{ galaxy_log_dir }}/set_user_disk_usage.log
          "

- name: galaxy logrotate
  hosts: galaxy
  tasks:
    - name: logrotate var/log/*.log
      template:
        src: logrotate/galaxy.j2
        dest: /etc/logrotate.d/galaxy

- name: galaxy_nginx logrotate
  hosts: galaxy_nginx
  tasks:
    - name: logrotate /var/log/proftpd/sql.log
      template:
        src: logrotate/galaxy_nginx.j2
        dest: /etc/logrotate.d/galaxy

- name: grafana sentry
  hosts: grafana
  become: true
  pre_tasks:
    - name: Check if Python 3.9 is already installed
      stat:
        path: /usr/local/bin/python3.9
      register: python_installed
    - name: Install Python 3.9 and pip3.9
      apt:
        name:
          - python3.9
        state: present
      when: python_installed.stat.exists == false
      
- name: grafana sentry
  hosts: grafana
  become: true
  vars:
    ansible_python_interpreter: /usr/local/bin/python3.9
  pre_tasks:
    - name: Check if Docker Compose is already installed
      shell: docker-compose --version 
      register: docker_compose_installed
      ignore_errors: yes

    - name: Install Docker Compose
      get_url:
        url: "https://github.com/docker/compose/releases/latest/download/docker-compose-{{ ansible_system }}-{{ 'x86_64' if ansible_architecture == 'x86_64' else 'arm64' }}"
        dest: /usr/local/bin/docker-compose
        mode: '0755'
      when: docker_compose_installed.failed

    - name: Install Docker Compose
      get_url:
        url: "https://github.com/docker/compose/releases/latest/download/docker-compose-Linux-x86_64"
        dest: /usr/local/bin/docker-compose
        mode: '0755'
      ignore_errors: yes
    
    - name: Check docker
      command: systemctl is-active docker
      register: docker_status
      changed_when: false

    - name: Restart Docker
      shell: systemctl restart docker
      when: docker_status.stdout != "active" 
      
  roles:
    - mvdbeek.sentry_selfhosted